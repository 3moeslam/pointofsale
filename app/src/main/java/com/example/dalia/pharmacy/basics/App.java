package com.example.dalia.pharmacy.basics;

import android.app.Application;
import android.graphics.Typeface;

import com.google.firebase.FirebaseApp;

import timber.log.Timber;

public class App extends Application {
    private static final String CANARO_EXTRA_BOLD_PATH = "fonts/canaro_extra_bold.otf";
    public static Typeface canaroExtraBold;
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
        Timber.plant(new Timber.DebugTree());
        initTypeface();
    }

    private void initTypeface() {
        canaroExtraBold = Typeface.createFromAsset(getAssets(), CANARO_EXTRA_BOLD_PATH);
    }
}
