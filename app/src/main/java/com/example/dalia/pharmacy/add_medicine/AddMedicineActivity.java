package com.example.dalia.pharmacy.add_medicine;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.DataProvider;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMedicineActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.root)
    ViewGroup root;
    @BindView(R.id.content_hamburger)
    View contentHamburger;


    @BindView(R.id.medicine_name)
    EditText medicineName;
    @BindView(R.id.medicine_price)
    EditText medicinePrice;
    @BindView(R.id.medicine_expire_date)
    EditText medicineExpireDate;
    @BindView(R.id.medicine_quantity)
    EditText medicineQuantity;


    @BindView(R.id.medicine_name_wrapper)
    TextInputLayout medicineNameWrapper;
    @BindView(R.id.medicine_price_wrapper)
    TextInputLayout medicinePriceWrapper;
    @BindView(R.id.medicine_expire_date_wrapper)
    TextInputLayout medicineExpireDateWrapper;
    @BindView(R.id.medicine_quantity_wraper)
    TextInputLayout medicineQuantityWraper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medicine);
        ButterKnife.bind(this);
        activityTitle.setText(R.string.add_medicine);

        setupMenu(root, toolbar, contentHamburger);


    }

    @OnClick({R.id.cancel_action, R.id.save_action})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel_action:
                break;
            case R.id.save_action:
                if(validateInputs()){
                    createMedicineItem();
                }

                break;
        }
    }

    private void createMedicineItem() {
        MedicensItem item = new MedicensItem();
        try {
            item.setPrice(Float.valueOf(medicinePrice.getText().toString()));
        }catch (Exception ex){
            medicinePriceWrapper.setError("Please Enter Valid number");
            return;
        }
        try{
            item.setQuantaty(Integer.valueOf(medicineQuantity.getText().toString()));
        }catch (Exception ex){
            medicineQuantityWraper.setError("Please Enter valid number");
            return;
        }

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(medicineExpireDate.getText().toString());
            item.setExpireDate(date.getTime());
        }catch (Exception ex){
            medicineExpireDateWrapper.setError("Please Enter valid date");
            return;
        }
        item.setTitle(medicineName.getText().toString());

        DataProvider.addMedicineItem(item);

        new AlertDialog.Builder(this)
                .setTitle("Done")
                .setMessage("Medicine Item added successfully, do you need to add new item?")
                .setPositiveButton("new item",this::clearAllItems)
                .setNegativeButton("go to home",this::gotoHomeScreen)
                .create()
                .show();
    }

    private void gotoHomeScreen(DialogInterface dialog, int which) {
        this.finish();
    }

    private void clearAllItems(DialogInterface dialog, int which) {
        medicineName.setText("");
        medicineQuantity.setText("");
        medicineExpireDate.setText("");
        medicinePrice.setText("");
        setNoErrors();
    }

    private boolean validateInputs() {
        setNoErrors();
        return validateTextEidtorsNotEmpty();
    }

    private boolean validateTextEidtorsNotEmpty() {
        boolean isValid = true;
        //Validate name
        if(TextUtils.isEmpty(medicineName.getText().toString())) {
            medicineNameWrapper.setError("Medicine name must not be empty");
            isValid = false;
        }
        //Validate Expire date
        if(TextUtils.isEmpty(medicineExpireDate.getText().toString())) {
            medicineExpireDateWrapper.setError("Medicine expire date must not be empty");
            isValid = false;
        }
        //Validate Expire date
        if(TextUtils.isEmpty(medicinePrice.getText().toString())) {
            medicinePriceWrapper.setError("Medicine price must not be empty");
            isValid = false;
        }
        //Validate Expire date
        if(TextUtils.isEmpty(medicineQuantity.getText().toString())) {
            medicineQuantityWraper.setError("Medicine quantity date must not be empty");
            isValid = false;
        }
        return isValid;
    }

    private void setNoErrors() {
        medicineNameWrapper.setError("");
        medicineExpireDateWrapper.setError("");
        medicinePriceWrapper.setError("");
        medicineQuantityWraper.setError("");
    }
}
