package com.example.dalia.pharmacy.add_sales;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.SalesItem;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.DataProvider;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class AddSalesActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.root)
    ViewGroup root;
    @BindView(R.id.content_hamburger)
    View contentHamburger;

    @BindView(R.id.all_medicines_sales)
    RecyclerView allMedicinesSales;
    @BindView(R.id.selected_medicines_sales)
    RecyclerView selectedMedicinesSales;

    @BindView(R.id.total_price)
    TextView totalPrice;

    BehaviorSubject<List<MedicensItem>> selectedItems = BehaviorSubject.create();
    Observable<List<MedicensItem>> allItems = DataProvider.getAllMedicens();
    SalesAllMedicines adaptor;
    @BindView(R.id.done)
    Button done;
    @BindView(R.id.search_bar)
    Toolbar searchBar;


    SelectedItemsSalesAdaptor selectedItemsAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sales);
        ButterKnife.bind(this);
        activityTitle.setText(R.string.sale_item);
        setupMenu(root, toolbar, contentHamburger);


        adaptor = new SalesAllMedicines(allItems, selectedItems);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        allMedicinesSales.setAdapter(adaptor);
        allMedicinesSales.setLayoutManager(layoutManager);

        selectedItemsAdaptor = new SelectedItemsSalesAdaptor(selectedItems, totalPrice);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        selectedMedicinesSales.setAdapter(selectedItemsAdaptor);
        selectedMedicinesSales.setLayoutManager(layoutManager1);
    }

    @OnClick(R.id.done)
    public void onViewClicked() {
        if (selectedItems.getValue().size() == 0) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage("Sorry you do not have any item in selected item please " +
                            "select items to confirm your order")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
        } else {
            SalesItem salesItem = new SalesItem();
            salesItem.setDate(Calendar.getInstance().getTime().getTime());
            salesItem.setMedicensItems(selectedItems.getValue());
            salesItem.setPrice(selectedItemsAdaptor.getTotal());
            DataProvider.insertSalesItem(salesItem);
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage("Done")
                    .setPositiveButton("Okay", (dialog1, which) -> {
                        dialog1.dismiss();
                        this.finish();
                    }).create();
            dialog.show();
        }
    }
}
