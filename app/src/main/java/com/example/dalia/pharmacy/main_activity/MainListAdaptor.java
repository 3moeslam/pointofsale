package com.example.dalia.pharmacy.main_activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.DataProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainListAdaptor extends RecyclerView.Adapter<MainListAdaptor.ViewItem> {

    private final List<MedicensItem> list = new ArrayList<>();

    @SuppressLint("CheckResult")
    MainListAdaptor(Observable<List<MedicensItem>> observable){
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onItemsChanged);
    }

    private void onItemsChanged(List<MedicensItem> list){
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        return new ViewItem(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewItem holder, int position) {
        holder.itemTitle.setText(list.get(position).getTitle());
        holder.priceText.setText("Pri. "+list.get(position).getPrice());
        holder.quantityText.setText("Qua. "+list.get(position).getQuantaty());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewItem extends RecyclerView.ViewHolder{
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.quantity_txt)
        TextView quantityText;
        @BindView(R.id.item_root)
        CardView root;
        ViewItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            root.setOnLongClickListener(v -> {
                AlertDialog dialog = new AlertDialog.Builder(root.getContext())
                        .setTitle("Remove Item")
                        .setMessage("Do you need to remove this item")
                        .setPositiveButton("Okay", (dialog1, which) -> {
                            list.remove(getAdapterPosition()-1);
                            List<MedicensItem> i =new ArrayList<>(list);
                            DataProvider.deleteItem(list.get(getAdapterPosition()-1));
                            notifyDataSetChanged();
                        }).setNegativeButton("Cancel",
                                (dialog12, which) -> dialog12.dismiss()).create();
                dialog.show();
                return true;
            });
        }
    }
}
