package com.example.dalia.pharmacy.top_sales;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TopSalesAdaptor extends RecyclerView.Adapter<TopSalesAdaptor.ItemViewHolder> {

    private final List<Pair<String, Integer>> list = new ArrayList<>();

    @SuppressLint("CheckResult")
    public TopSalesAdaptor(Observable<List<MedicensItem>> mediceneObservable) {
        mediceneObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNewDataIncomming);
    }

    private void onNewDataIncomming(List<MedicensItem> list) {
        HashMap<String, Pair<String, Integer>> map = new HashMap<>();
        for (MedicensItem item : list) {
            if (map.containsKey(item.getId())) {
                Pair<String, Integer> pair = map.get(item.getId());
                pair.setSecond(pair.getSecond() + 1);
            } else {
                Pair<String, Integer> newPair = new Pair<>(item.getTitle(), 1);
                map.put(item.getId(), newPair);
            }
        }
        List<Pair<String, Integer>> finalList = new ArrayList<>(map.values());
        Collections.sort(finalList, (r, ss) ->
                Integer.compare(r.getSecond(), ss.getSecond()) * -1
        );
        this.list.clear();
        this.list.addAll(finalList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new TopSalesAdaptor.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.itemTitle.setText(list.get(position).getFirst());
        holder.priceText.setText("Sold items: "+list.get(position).getSecond());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.quantity_txt)
        TextView quantityText;
        @BindView(R.id.item_root)
        CardView root;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            quantityText.setVisibility(View.GONE);
        }
    }
}
