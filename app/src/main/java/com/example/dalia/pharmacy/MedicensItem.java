package com.example.dalia.pharmacy;

public class MedicensItem implements Comparable<MedicensItem>{
    private String id;
    private String title;
    private float price;
    private int quantaty;
    private long expireDate;

    public MedicensItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(long expireDate) {
        this.expireDate = expireDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantaty() {
        return quantaty;
    }

    public void setQuantaty(int quantaty) {
        this.quantaty = quantaty;
    }


    @Override
    public int compareTo(MedicensItem o) {
        return title.compareTo(o.getTitle());
    }
}
