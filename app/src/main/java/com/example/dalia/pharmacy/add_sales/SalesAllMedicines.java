package com.example.dalia.pharmacy.add_sales;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class SalesAllMedicines extends RecyclerView.Adapter<SalesAllMedicines.ItemViewHolder> {

    private final List<MedicensItem> list = new ArrayList<>();
    private List<MedicensItem> selectedList = new ArrayList<>();
    private BehaviorSubject<List<MedicensItem>> selectedObservable;
    Observable<List<MedicensItem>> observable;
    @SuppressLint("CheckResult")
    SalesAllMedicines(Observable<List<MedicensItem>> observable, BehaviorSubject<List<MedicensItem>> selectedItems) {
        this.observable = observable;
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onItemsChanged);
        selectedObservable = selectedItems;
        selectedObservable.onNext(new ArrayList<>());
    }

    private void onItemsChanged(List<MedicensItem> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new SalesAllMedicines.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.itemTitle.setText(list.get(position).getTitle());
        holder.rootView.setOnClickListener((v) -> {
            selectedList = selectedObservable.getValue();
            selectedList.add(list.get(position));
            selectedObservable.onNext(selectedList);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.quantity_txt)
        TextView quantityText;
        @BindView(R.id.item_root)
        CardView rootView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            priceText.setVisibility(View.GONE);
            quantityText.setVisibility(View.GONE);
        }
    }
}
