package com.example.dalia.pharmacy.needed_medicenes;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.CanaroTextView;
import com.example.dalia.pharmacy.basics.DataProvider;
import com.example.dalia.pharmacy.top_sales.TopSalesAdaptor;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class NeededMedicense extends BaseActivity {

    @BindView(R.id.content_hamburger)
    ImageView contentHamburger;
    @BindView(R.id.activity_title)
    CanaroTextView activityTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.needed_medicine)
    RecyclerView neededMedicine;
    @BindView(R.id.root)
    ConstraintLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_needed_medicense);
        ButterKnife.bind(this);
        Timber.i("NeededMedicense onCreate");
        activityTitle.setText("Needed Medicense");
        setupMenu(root, toolbar, contentHamburger);
        initializeRecyclerView();
    }


    private void initializeRecyclerView() {
        NeededMedicensAdaptor adaptor = new NeededMedicensAdaptor(DataProvider.getNeededMedicens(20));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        neededMedicine.setAdapter(adaptor);
        neededMedicine.setLayoutManager(layoutManager);
    }


}
