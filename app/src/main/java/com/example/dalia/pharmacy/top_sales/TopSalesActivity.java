package com.example.dalia.pharmacy.top_sales;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.CanaroTextView;
import com.example.dalia.pharmacy.basics.DataProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopSalesActivity extends BaseActivity {

    @BindView(R.id.content_hamburger)
    ImageView contentHamburger;
    @BindView(R.id.activity_title)
    CanaroTextView activityTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.root)
    ConstraintLayout root;
    @BindView(R.id.sales_medicine)
    RecyclerView salesMedicine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_sales);
        ButterKnife.bind(this);
        activityTitle.setText("Top Sales");
        setupMenu(root, toolbar, contentHamburger);
        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        TopSalesAdaptor adaptor = new TopSalesAdaptor(DataProvider.getSoldMedcineItems());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        salesMedicine.setAdapter(adaptor);
        salesMedicine.setLayoutManager(layoutManager);

    }
}
