package com.example.dalia.pharmacy.expire_date_activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.CanaroTextView;
import com.example.dalia.pharmacy.basics.DataProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpireMedicence extends BaseActivity {

    @BindView(R.id.content_hamburger)
    ImageView contentHamburger;
    @BindView(R.id.activity_title)
    CanaroTextView activityTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.expire_medicine)
    RecyclerView expireMedicine;
    @BindView(R.id.root)
    ConstraintLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expire_medicence);
        ButterKnife.bind(this);
        activityTitle.setText("Expire Medicines");
        setupMenu(root, toolbar, contentHamburger);
        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        ExpireMedicenceAdaptor adaptor = new ExpireMedicenceAdaptor(DataProvider.getExpiredMedicens());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        expireMedicine.setAdapter(adaptor);
        expireMedicine.setLayoutManager(layoutManager);
    }
}
