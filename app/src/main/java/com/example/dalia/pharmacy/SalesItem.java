package com.example.dalia.pharmacy;

import java.util.List;

public class SalesItem {
    private String id;
    private long date;
    private float price;
    private List<MedicensItem> medicensItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<MedicensItem> getMedicensItems() {
        return medicensItems;
    }

    public void setMedicensItems(List<MedicensItem> medicensItems) {
        this.medicensItems = medicensItems;
    }
}
