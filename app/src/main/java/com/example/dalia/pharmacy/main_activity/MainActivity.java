package com.example.dalia.pharmacy.main_activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.basics.BaseActivity;
import com.example.dalia.pharmacy.basics.DataProvider;
import com.example.dalia.pharmacy.basics.Pair;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_title)
    TextView activityTitle;
    @BindView(R.id.root)
    ViewGroup root;
    @BindView(R.id.main_list)
    RecyclerView mainList;

    @BindView(R.id.content_hamburger)
    View contentHamburger;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        activityTitle.setText("All Items");
        setupMenu(root, toolbar, contentHamburger);

        MainListAdaptor adaptor = new MainListAdaptor(DataProvider.getAllMedicens());
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mainList.setAdapter(adaptor);
        mainList.setLayoutManager(layoutManager);

        DataProvider.getSoldMedcineItems()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(list -> {
                    HashMap<String, Pair<String, Integer>> map = new HashMap<>();
                    for (MedicensItem item : list) {
                        if (map.containsKey(item.getId())) {
                            Pair<String, Integer> pair = map.get(item.getId());
                            pair.setSecond(pair.getSecond() + 1);
                        } else {
                            Pair<String, Integer> newPair = new Pair<>(item.getTitle(), 1);
                            map.put(item.getId(), newPair);
                        }
                    }
                    List<Pair<String, Integer>> l = new ArrayList<>(map.values());
                    Collections.sort(l, (r, ss) ->
                            Integer.compare(r.getSecond(), ss.getSecond()) *-1
                    );
                    for (Pair<String, Integer> pair : l) {
                        Timber.i("Item is: %s , items count is: %d", pair.getFirst(), pair.getSecond());
                    }
                });
    }
}
