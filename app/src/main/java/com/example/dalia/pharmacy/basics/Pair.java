package com.example.dalia.pharmacy.basics;

import android.support.annotation.Nullable;

public class Pair<F,S> {

    private @Nullable F first;
    private @Nullable S second;

    public Pair(F first,S second){
        this.first = first;
        this.second = second;
    }

    @Nullable
    public F getFirst() {
        return first;
    }

    @Nullable
    public S getSecond() {
        return second;
    }

    public void setFirst(@Nullable F first) {
        this.first = first;
    }

    public void setSecond(@Nullable S second) {
        this.second = second;
    }

}
