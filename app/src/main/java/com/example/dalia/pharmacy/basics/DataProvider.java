package com.example.dalia.pharmacy.basics;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.SalesItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import timber.log.Timber;

public class DataProvider {
    private static ExecutorService poolExecutor;

    static {
        poolExecutor = Executors.newSingleThreadScheduledExecutor();
    }

    public static Observable<List<MedicensItem>> getAllMedicens() {
        return Observable.create(emitter -> {
            FirebaseDatabase.getInstance().getReference("medicens")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                List<MedicensItem> items = new ArrayList<>();
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    MedicensItem item = child.getValue(MedicensItem.class);
                                    if (item == null) continue;
                                    item.setId(child.getKey());
                                    items.add(item);
                                }
                                emitter.onNext(items);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
        });
    }

    public static void addMedicineItem(MedicensItem item) {
        poolExecutor.execute(() -> {
            String key = FirebaseDatabase.getInstance().getReference("medicens").push().getKey();
            FirebaseDatabase.getInstance().getReference("medicens")
                    .child(key)
                    .setValue(item, (databaseError, dataRef) -> {

                    });
        });
    }


    public static void insertSalesItem(SalesItem item) {
        poolExecutor.execute(() -> {
            String key = FirebaseDatabase.getInstance().getReference("sales").push().getKey();
            FirebaseDatabase.getInstance().getReference("sales")
                    .child(key)
                    .setValue(item);
        });
    }

    public static Observable<List<SalesItem>> getSalesItems() {
        return Observable.create(emitter -> {
            FirebaseDatabase.getInstance().getReference("sales")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                List<SalesItem> items = new ArrayList<>();
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    SalesItem item = child.getValue(SalesItem.class);
                                    if (item == null) continue;
                                    item.setId(child.getKey());
                                    items.add(item);
                                }
                                emitter.onNext(items);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
        });
    }

    public static Observable<List<MedicensItem>> getSoldMedcineItems() {
        return Observable.create(emitter -> {
            FirebaseDatabase.getInstance().getReference("sales")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                List<MedicensItem> list = new ArrayList<>();
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    SalesItem item = child.getValue(SalesItem.class);
                                    if (item == null) continue;
                                    item.setId(child.getKey());
                                    for (MedicensItem medicensItem : item.getMedicensItems()) {
                                        list.add(medicensItem);
                                    }
                                    ;
                                }
                                emitter.onNext(list);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
        });
    }

    public static void getTopNSales(int number) {
        getSalesItems()
                .groupBy(list -> list.get(0))
                .flatMap(g -> g.map(i -> {
                    Timber.i("item is: " + g.getKey());
                    return i;
                }))
                .subscribe(item -> Timber.i("Item in observable is: " + item));
    }

    public static Observable<List<MedicensItem>> getExpiredMedicens() {
        return Observable.create(observer ->
                FirebaseDatabase.getInstance().getReference("medicens")
                        .orderByChild("expireDate")
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (!observer.isDisposed()) {
                                    if (dataSnapshot.exists()) {
                                        List<MedicensItem> list = new ArrayList<>();
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            MedicensItem item = snapshot.getValue(MedicensItem.class);
                                            list.add(item);
                                        }
                                        observer.onNext(list);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        })
        );
    }

    public static Observable<List<MedicensItem>> getNeededMedicens(int quantity) {
        return Observable.create(observer ->
                FirebaseDatabase.getInstance().getReference("medicens")
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (!observer.isDisposed()) {
                                    if (dataSnapshot.exists()) {
                                        List<MedicensItem> list = new ArrayList<>();
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            MedicensItem item = snapshot.getValue(MedicensItem.class);
                                            if (item.getQuantaty() < quantity)
                                                list.add(item);
                                        }
                                        observer.onNext(list);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        })
        );
    }

    public static void deleteItem(MedicensItem item){
        FirebaseDatabase.getInstance().getReference("medicens")
                .child(item.getId())
                .removeValue();
    }
}
