package com.example.dalia.pharmacy.needed_medicenes;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.expire_date_activity.ExpireMedicenceAdaptor;
import com.example.dalia.pharmacy.top_sales.TopSalesAdaptor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NeededMedicensAdaptor extends RecyclerView.Adapter<NeededMedicensAdaptor.ItemViewHolder>{

    private final List<MedicensItem> list = new ArrayList<>();


    @SuppressLint("CheckResult")
    NeededMedicensAdaptor(Observable<List<MedicensItem>> observable) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNewData);
    }

    private void onNewData(List<MedicensItem> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NeededMedicensAdaptor.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new NeededMedicensAdaptor.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NeededMedicensAdaptor.ItemViewHolder holder, int position) {
        holder.itemTitle.setText(list.get(position).getTitle());
        holder.priceText.setText("Quantity: "+list.get(position).getQuantaty());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.quantity_txt)
        TextView quantityText;
        @BindView(R.id.item_root)
        CardView root;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            quantityText.setVisibility(View.GONE);
        }
    }
}
