package com.example.dalia.pharmacy.basics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.dalia.pharmacy.add_medicine.AddMedicineActivity;
import com.example.dalia.pharmacy.R;
import com.example.dalia.pharmacy.add_sales.AddSalesActivity;
import com.example.dalia.pharmacy.expire_date_activity.ExpireMedicence;
import com.example.dalia.pharmacy.main_activity.MainActivity;
import com.example.dalia.pharmacy.needed_medicenes.NeededMedicense;
import com.example.dalia.pharmacy.top_sales.TopSalesActivity;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

    private static final long RIPPLE_DURATION = 250;
    private GuillotineAnimation menu;


    public void setupMenu(ViewGroup root, Toolbar toolbar, View contentHamburger) {
        View menuView = LayoutInflater.from(this).inflate(R.layout.app_menu, null);
        menuView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        root.addView(menuView);
        menu = new GuillotineAnimation.GuillotineBuilder(menuView,
                menuView.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
        menuView.findViewById(R.id.home_menu).setOnClickListener(this::onMenuItemSelected);
        menuView.findViewById(R.id.add_item_menu).setOnClickListener(this::onMenuItemSelected);
        menuView.findViewById(R.id.add_sales_item).setOnClickListener(this::onMenuItemSelected);
        menuView.findViewById(R.id.top_sales_activity).setOnClickListener(this::onMenuItemSelected);
        menuView.findViewById(R.id.expire_medicine).setOnClickListener(this::onMenuItemSelected);
        menuView.findViewById(R.id.nedded_medicine).setOnClickListener(this::onMenuItemSelected);
    }

    public void onMenuItemSelected(View view) {
        switch (view.getId()){
            case R.id.home_menu:
                startNewActivity(MainActivity.class);
                if(!(this instanceof MainActivity)){
                    this.finish();
                }
                break;
            case R.id.add_item_menu:
                startNewActivity(AddMedicineActivity.class);
                break;
            case R.id.add_sales_item:
                startNewActivity(AddSalesActivity.class);
                break;
            case R.id.top_sales_activity:
                startNewActivity(TopSalesActivity.class);
                break;
            case R.id.expire_medicine:
                startNewActivity(ExpireMedicence.class);
                break;
            default:
                startNewActivity(NeededMedicense.class);
                Timber.i("NeededMedicense");
                break;
        }
        closeMenu();
    }


    public void startNewActivity(Class targetActivity) {
        Intent i = new Intent(this,targetActivity);
        startActivity(i);
    }
    public void closeMenu(){
        menu.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
