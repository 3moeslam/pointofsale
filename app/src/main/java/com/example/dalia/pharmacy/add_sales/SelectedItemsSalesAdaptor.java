package com.example.dalia.pharmacy.add_sales;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dalia.pharmacy.MedicensItem;
import com.example.dalia.pharmacy.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class SelectedItemsSalesAdaptor extends RecyclerView.Adapter<SelectedItemsSalesAdaptor.ItemViewHolder> {


    private final List<MedicensItem> list = new ArrayList<>();
    TextView totalPrice;
    BehaviorSubject<List<MedicensItem>> observable;

    private float total = 0;
    @SuppressLint("CheckResult")
    SelectedItemsSalesAdaptor(BehaviorSubject<List<MedicensItem>> observable,TextView totalPrice){
        this.observable = observable;
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onItemsChanged);
        this.totalPrice = totalPrice;
    }

    private void onItemsChanged(List<MedicensItem> list){
        this.list.clear();
        this.list.addAll(list);
        total = 0;
        for (MedicensItem item:list) {
            item.setQuantaty(1);
            total+=item.getPrice();
        }
        totalPrice.setText("Total Price: "+total);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        int height = parent.getMeasuredHeight();
        view.setMinimumHeight(height);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = parent.getWidth()/2;
        view.setLayoutParams(params);
        return new SelectedItemsSalesAdaptor.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.itemTitle.setText(list.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public float getTotal() {
        return total;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.quantity_txt)
        TextView quantityText;
        @BindView(R.id.item_root)
        CardView root;
        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            priceText.setVisibility(View.GONE);
            quantityText.setVisibility(View.GONE);

            root.setOnLongClickListener(v -> {
                AlertDialog dialog = new AlertDialog.Builder(root.getContext())
                        .setTitle("Remove Item")
                        .setMessage("Do you need to remove this item")
                        .setPositiveButton("Okay", (dialog1, which) -> {
                            list.remove(getAdapterPosition());
                            List<MedicensItem> i =new ArrayList<>(list);
                            observable.onNext(i);
                        }).setNegativeButton("Cancel",
                                (dialog12, which) -> dialog12.dismiss()).create();
                dialog.show();
                return true;
            });
        }
    }
}
